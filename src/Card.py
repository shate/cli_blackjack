class Card:
    point_generator = {"One": 1, "Two": 2, "Three": 3, "Four": 4, "Five": 5, "Six": 6, "Seven": 7, "Eight": 8,
                       "Nine": 9, "Ten": 10, "Jack": 10, "Queen": 10, "King": 10, "Ace": [1, 11]}

    def __init__(self, figure, color):
        self.figure = figure
        self.color = color
        self.points = Card.point_generator[self.figure]
        self.hidden = False

    def __repr__(self):
        return self.figure + " of " + self.color
