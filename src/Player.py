from src.Statistic import Statistic
from src.Hand import Hand


class Player:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name
        self.amount = 100
        self.passed = False
        self.hand = Hand()
        self.statistic = Statistic()

    def check_amount(self, bet):
        if self.amount < bet:
            return False
        else:
            return True

    def win(self):
        self.statistic.wins += 1

    def lose(self):
        self.statistic.losses += 1

    def bust(self):
        self.statistic.busts += 1

    def bet(self):
        while True:
            try:
                amount = int(input("Write bet amount: "))
            except TypeError:
                print("You must enter a number")
            else:
                if self.check_amount(amount):
                    return amount
                else:
                    print(f"Entered amount ({amount}) is bigger than your account status ({self.amount})")
                    continue
