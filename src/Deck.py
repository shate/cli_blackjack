import random
from src.Card import Card


class Deck:
    def __init__(self):
        colors = ["Hearts", "Diamonds", "Clubs", "Spades"]
        figures = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen",
                   "King", "Ace"]
        self.deck = []
        for color in colors:
            for figure in figures:
                self.deck.append(Card(figure, color))

    def shuffle(self):
        random.shuffle(self.deck)

    def new_deck(self):
        self.__init__()

