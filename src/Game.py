from src.Player import Player
from src.Deck import Deck
from src.Hand import Hand


class Game:
    def __init__(self, player):
        self.player = player
        self.croupier = Player("Croupier", "Croupy")
        self.deck = Deck()
        self.game_on = True
        self.bet = 0

    def info(self):
        print("-------------------------------------------------------------------------------------------------------")
        print(f" Current bet: {game.bet} $".center(100))
        print("-------------------------------------------------------------------------------------------------------")
        player_info = f"Player {self.player.first_name} (Account: {self.player.amount}$): \n Hand: {self.player.hand} \n Points: {self.player.hand.points}\n\n"
        croupier_info = f"Player {self.croupier.first_name}: \n Hand: {self.croupier.hand} \n Points: {self.croupier.hand.points}"
        print(player_info + croupier_info)
        print("-------------------------------------------------------------------------------------------------------")

    def check_win(self):
        if self.player.hand.bust or self.player.passed and self.croupier.hand.points > self.player.hand.points and not self.croupier.hand.bust:
            self.croupier_win()
            return True
        elif self.croupier.hand.bust:
            self.player_win()
            return True
        else:
            return False

    def player_win(self):
        self.info()
        game.player.amount += game.bet
        game.bet = 0
        self.player.win()
        print(f"Congratulations, you won! (Your account status: {self.player.amount}$)")

    def croupier_win(self):
        self.info()
        game.player.amount -= game.bet
        game.bet = 0
        self.player.lose()
        print(f"Game over, croupier won (Your account status: {self.player.amount}$)")

    def draw_card(self, player):
        player.hand.draw_card(game.deck.deck.pop())

    def player_move(self):
        while True:
            try:
                move = int(input("[1] Stand           [2] Hit\nMove: "))
                if move not in [1, 2]:
                    raise Exception
            except TypeError:
                print("You must enter a number")
            except:
                print("You must chose [1] or [2]")
            else:
                if move == 1:
                    game.player.passed = True
                    print("PASS")
                elif move == 2:
                    print("HIT")
                    self.player.hand.draw_card(self.deck.deck.pop())
                break

    def new_game(self):
        while True:
            try:
                decision = int(input("[1] Next game             [2] END                [3] Show stats\nDecision: "))
            except TypeError:
                print("You must enter a number")
            else:
                if decision == 3:
                    print(self.player.statistic)
                elif decision == 1:
                    self.deck.new_deck()
                    self.player.hand = Hand()
                    self.croupier.hand = Hand()
                    self.player.passed = False
                    self.croupier.passed = False
                    break
                else:
                    self.game_on = False
                    break

    def croupier_move(self):
        self.croupier.hand.draw_card(self.deck.deck.pop())


if __name__ == "__main__":
    player = Player("Skarzt", "Maly")
    game = Game(player)

    while game.game_on:
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~  WELCOME TO BLACKJACK ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        game.deck.shuffle()
        game.draw_card(player)
        game.draw_card(player)
        game.draw_card(game.croupier)
        game.draw_card(game.croupier)
        game.croupier.hand.cards[1].hidden = True
        game.bet = game.player.bet()
        if game.player.hand.points != 21:
            while not game.check_win():
                game.info()
                if not game.player.passed:
                    game.player_move()
                game.croupier.hand.cards[1].hidden = False
                game.croupier_move()
            else:
                game.new_game()
        else:
            print("$$$$$$               BLACKJACK               $$$$$$")
            game.player_win()
            game.new_game()
