class Hand:
    def __init__(self):
        self.cards = []
        self.points = 0
        self.bust = False

    def draw_card(self, card):
        self.cards.append(card)
        self.check_points()

    def check_points(self):
        total = 0
        for card in self.cards:
            if not card.hidden:
                if card.figure == "Ace":
                    if total + card.points[1] > 21:
                        total += card.points[0]
                    else:
                        total += card.points[1]
                else:
                    total += card.points
        if total > 21:
            self.bust = True
        self.points = total

    def __repr__(self):
        info = ""
        for i, card in enumerate(self.cards):
            if i != 0:
                info += ", "
            if card.hidden:
                info += "Hidden"
            else:
                info += card.__repr__()
        return info
