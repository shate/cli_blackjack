
class Statistic:
    def __init__(self):
        self.wins = 0
        self.losses = 0
        self.busts = 0

    def __repr__(self):
        return f"Wins: {self.wins}\nLosses: {self.losses}"
